function themeIsReverse(){
    if(localStorage.getItem("reverse")){
        return true;
    }
    return false;
}

function applyTheme(){
    if(themeIsReverse()){
        document.body.classList.add("reverse");
    }else{
        document.body.classList.remove("reverse");
    }
}

function toggleTheme(){
    if(themeIsReverse()){
        localStorage.removeItem("reverse");
    }else{
        localStorage.setItem("reverse", 1);
    }
    applyTheme();
}

async function loadAnnouncement() {
	const request = await fetch('/update.json');
    const response = await request.json();
	if (response.get("show")) {
		if (localStorage.getItem('announcement-hidden')) {
			document.getElementById("announcement-banner").style.display = "block";
		}
	}
}

function hideAnnouncement() {
	localStorage.setItem('announcement-hidden', 1);
	document.getElementById("announcement-banner").style.display = "none";
}